{ pkgs ? fetchTarball https://github.com/NixOS/nixpkgs/archive/43c77db3aa58e06cc3ced846431dd4228f93cd5d.tar.gz
}:

with import pkgs {};

let

  nodeEnv = import ./nix { };

  builds = { inherit (nodeEnv) tarball package; };

in if stdenv.lib.inNixShell then nodeEnv.shell else builds
